<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'PagesController@home')->name('home');
Route::get('/news', 'PagesController@news')->name('news.home');
Route::get('/changelog', 'PagesController@changelog')->name('changelog');
Route::get('/dashboard', 'PagesController@dashboard')->name('dashboard');

Route::get('/news/create', 'PostsController@create')->name('post.new');
Route::post('/news', 'PostsController@store')->name('post.publish');
Route::get('/news/{post}', 'PostsController@show')->name('post.view');
Route::get('/news/{post}/edit', 'PostsController@edit')->name('post.edit');
Route::put('/news/{post}', 'PostsController@update')->name('post.update');
Route::delete('/news/{post}', 'PostsController@destroy')->name('post.delete');

Route::get('/account/settings', 'AccountsController@editSettings')->name('account.settings');
Route::get('/u/{user}', 'AccountsController@show')->name('account.profile');
Route::put('/account/{action}', 'AccountsController@update')->name('account.update');
