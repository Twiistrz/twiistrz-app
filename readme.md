Twiistrz APP
=======

[![Build Status](https://travis-ci.org/Twiistrz/Twiistrz-App.svg?branch=master)](https://travis-ci.org/Twiistrz/Twiistrz-App) [![StyleCI](https://styleci.io/repos/134821316/shield)](https://styleci.io/repos/134821316) [![TLIBR](https://discordapp.com/api/guilds/442354079193104394/widget.png?style=shield)](https://discord.gg/ZBxkcU6)

Browse my new webiste to learn more.

Requirements
------------

- A PHP 7.1 (or equivalent) environment
- MySQL 5.7+

Deploying
---------

### Applications

- XAMPP
- Node JS
- Git Bash (Required)
- VSCODE (Optional)

### Quick Start

Create a database: `twiistrz.db`

```bash
# Install Dependencies
$ composer install
$ npm install

# Run Migrations and Seed
$ php artisan migrate --seed

# Link storage
$ php artisan storage:link

# If you get an error about an encryption key
$ php artisan key:generate

# Publish Vendors
$ php artisan vendor:publish --tag=ckeditor

# Watch Files
$ npm run dev
```

Default User:

- Name: Default User
- Username: developer
- Email: developer@twiistrz.localhost
- Password: developer
- Role: Developer

Seeking Help
------------

If you need help with anything, you have three options:

### Contact Me

- [Facebook](https://www.facebook.com/Twiistrz)
- [Email](mailto:bsit.emmanuel@gmail.com)

### Creating an Issue

If you have something you want to discuss in detail, or have hit an issue which you believe others will also have in deployment or development on the system, [opening an issue](https://github.com/Twiistrz/CRUD-using-Laravel/issues) is the best way to get help. It creates a permanent resource for others wishing to contribute to conversation. Please **make sure to search first** in case someone else has already addressed the same issue!

Licence
-------

Twiistrz CRUD is licensed under AGPL version 3 or later. Please see [the licence file](LICENCE) for more information. [tl;dr](https://tldrlegal.com/license/gnu-affero-general-public-license-v3-(agpl-3.0)) if you want to use any code, design or artwork from this project, attribute it and make your project open source under the same licence.
