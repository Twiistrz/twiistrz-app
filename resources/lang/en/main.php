<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Main Language Lines
    |--------------------------------------------------------------------------
    */

    // Camel
    'account_settings'          => 'Account Settings',
    'update'                    => 'Update',
    'avatar'                    => 'Avatar',
    'change_your_avatar'        => 'Change your Avatar',
    'choose_avatar'             => 'Choose Avatar',
    'email'                     => 'Email',
    'password'                  => 'Password',
    'profile'                   => 'Profile',
    'c_about_me'                => 'About Me',
    'my_profile'                => 'My Profile',
    'settings'                  => 'Settings',
    'dashboard'                 => 'Dashboard',
    'logout'                    => 'Sign Out',
    'c_changelog'               => 'Changelog',
    'c_news'                    => 'News',
    'posted'                    => 'Posted ',
    'create_post'               => 'Create Post',
    'new_post'                  => 'New Post',
    'edit_post'                 => 'Edit Post',
    'publish_post'              => 'Publish Post',
    'update_post'               => 'Update Post',
    'view_post'                 => 'View Post',
    'edit_post'                 => 'Edit Post',
    'delete_post'               => 'Delete Post',
    'post_title'                => 'Post Title',
    'post_body'                 => 'Post Body',
    'choose_cover_image'        => 'Choose Cover Image',
    'new_issue'                 => 'New Issue',
    'joined'                    => 'Member Since ',

    // Lowercase
    'home'                      => 'home',
    'news'                      => 'news',
    'changelog'                 => 'changelog',
    'new_email'                 => 'new email',
    'email_confirmation'        => 'email confirmation',
    'current_password'          => 'current password',
    'new_password'              => 'new password',
    'password_confirmation'     => 'password confirmation',
    'name'                      => 'name',
    'username'                  => 'username',
    'current_location'          => 'current location',
    'interests'                 => 'interests',
    'occupation'                => 'occupation',
    'twitter'                   => 'twitter',
    'discord'                   => 'discord',
    'skype'                     => 'skype',
    'website'                   => 'website',
    'about_me'                  => 'about me',
    'total_news_posts'          => 'total news posts',

    // Paragraph
    'this_content_is_currently_unavailable'                 => 'This content is currently unavailable',
    'no_content_available_right_now'                        => 'No content available right now. It may be temporarily unavailable, or create a new post.',
    'the_page_you_request_cannot_be_displayed_right_now'    => 'The page you requested cannot be displayed right now. It may be temporarily unavailable, the link you clicked on may have expired, or you may not have permission to view this page.',

];
