@extends('master')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @if ($user)
                <div class="col-md-12">
                    <div class="text-center">
                        <div class="figure">
                            @if (empty($user->avatar))
                                @php
                                    $user->avatar = "data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22200%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20200%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_163a43326ef%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_163a43326ef%22%3E%3Crect%20width%3D%22200%22%20height%3D%22200%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2274.4296875%22%20y%3D%22104.5%22%3E200x200%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E";
                                @endphp
                            @else
                                @php
                                    $user->avatar = url('/storage/avatar/' . $user->avatar);
                                @endphp
                            @endif

                            @if (auth()->check() and auth()->user()->id == $user->id) <a href="{{ route('account.settings') }}" data-toggle="tooltip" data-placement="bottom" title="{{ __('main.change_your_avatar') }}">@endif
                            <img style="background-image: url('{{ $user->avatar }}');" class="figure-image mx-auto d-block avatar-profile border">
                            @if (auth()->check() and auth()->user()->id == $user->id) </a>@endif
                        </div>
                        <h3 class="d-block font-weight-light">{{ $user->name }}</h3>
                        <span class="badge badge-pill badge-primary">{{ $user->getRole($user->id) }}</span>
                        <small class="d-block font-weight-light">{{ __('main.joined') . \Carbon\Carbon::createFromTimeStamp(strtotime($user->created_at))->diffForHumans() }}</small>
                    </div>
                    <hr>
                </div>
                
                <div class="col-md-6">
                    <h3 class="text-center font-italic font-weight-light">{{ __('main.profile') }}</h3>
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <ul class="list-group">
                                <li class="list-group-item"><span class="font-weight-bold">{{ __('main.total_news_posts') }}</span>: {{ count($posts) }}</li>
                                @if (!empty($user->current_location))<li class="list-group-item"><span class="font-weight-bold">{{ __('main.current_location') }}</span>: {{ $user->current_location }}</li>@endif
                                @if (!empty($user->interest))<li class="list-group-item"><span class="font-weight-bold">{{ __('main.interests') }}</span>: {{ $user->interest }}</li>@endif
                                @if (!empty($user->occupation))<li class="list-group-item"><span class="font-weight-bold">{{ __('main.occupation') }}</span>: {{ $user->occupation }}</li>@endif
                            </ul>
                        </div>

                        @if (!empty($user->twitter) or !empty($user->discord) or !empty($user->skype) or !empty($user->website))
                            <div class="col-lg-6">
                                <ul class="list-group">
                                    @if (!empty($user->twitter))<li class="list-group-item"><span class="font-weight-bold">{{ __('main.twitter') }}</span>: <a href="{{ url('https://twitter.com/' . $user->twitter) }}" target="_blank">{{ _('@') . $user->twitter }}</a></li>@endif
                                    @if (!empty($user->discord))<li class="list-group-item"><span class="font-weight-bold">{{ __('main.discord') }}</span>: {{ $user->discord }}</li>@endif
                                    @if (!empty($user->skype))<li class="list-group-item"><span class="font-weight-bold">{{ __('main.skype') }}</span>: <a href="skype:{{ $user->skype }}?chat">{{ $user->skype }}</a></li>@endif
                                    @if (!empty($user->website))<li class="list-group-item"><span class="font-weight-bold">{{ __('main.website') }}</span>: <a href="{{ url($user->website) }}" target="_blank">{{ url($user->website) }}</a></li>@endif
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>

                @if (!empty($user->bio))
                    <div class="col-md-6">
                        <h3 class="text-center font-italic font-weight-light">{{ __('main.c_about_me') }}</h3>
                        <div class="card">
                            <div class="card-body" style="max-height:360px;overflow-y:auto;">
                                {!! $user->bio !!}
                            </div>
                        </div>
                    </div>
                @endif
            @else
                <div class="col-md-8">
                    @include('errors._show_not_available')
                </div>
            @endif
        </div>
    </div>
@endsection