@extends('master')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10 settings">
                <h3 class="text-center font-italic font-weight-light">{{ __('main.account_settings') }}</h3>
                <hr>
                <div class="row">
                    @include('accounts._edit_avatar')
                    @include('accounts._edit_profile')
                    @include('accounts._edit_password')
                    @include('accounts._edit_email')
                </div>        
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>CKEDITOR.replace('article-ckeditor');</script>
    @if (count($errors) > 0) 
        @include('errors._show_swal_error')  
    @endif
@endsection