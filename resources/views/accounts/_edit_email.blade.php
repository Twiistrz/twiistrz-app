<div class="col-md-12">
    {!! Form::open([
        'route'   => ['account.update', 'settings-email'], 
        'method'  => 'POST', 
        'class'   => 'needs-validation'
    ]) !!}
        <div class="card">
            <div class="card-body">
                <div class="row">

                    <div class="col-lg-4">
                        <h4 class="text-center font-italic font-weight-light">{{ __('main.email') }}</h4>
                    </div>

                    <div class="col-lg-8 border-left">
                        <div class="form-group row">
                            <label for="current_password_email" class="col-sm-4 col-form-label text-md-right">{{ __('main.current_password') }}</label>

                            <div class="col-md-8">
                                <input 
                                    type="password" 
                                    id="current_password_email" 
                                    name="current_password_email" 
                                    class="form-control{{ $errors->has('current_password_email') ? ' is-invalid' : '' }}" 
                                    autocomplete="off"
                                    {{ $errors->has('current_password_email') ? ' autofocus' : '' }}
                                >

                                @if ($errors->has('current_password_email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('current_password_email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <hr>

                        <div class="form-group row">
                            <label for="new_email" class="col-sm-4 col-form-label text-md-right">{{ __('main.new_email') }}</label>

                            <div class="col-md-8">
                                <input 
                                    type="email" 
                                    id="new_email" 
                                    name="new_email" 
                                    value="{{ $errors->has('new_email') ? old('new_email') : '' }}"
                                    class="form-control{{ $errors->has('new_email') ? ' is-invalid' : '' }}" 
                                    autocomplete="off"
                                    {{ $errors->has('new_email') ? ' autofocus' : '' }}
                                >

                                @if ($errors->has('new_email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('new_email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email_confirmation" class="col-sm-4 col-form-label text-md-right">{{ __('main.email_confirmation') }}</label>

                            <div class="col-md-8">
                                <input 
                                    type="email" 
                                    id="email_confirmation" 
                                    name="email_confirmation" 
                                    class="form-control" 
                                    autocomplete="off"
                                >
                            </div>
                        </div>

                        <hr>

                        <div class="form-group row">
                            <div class="col-md-4">
                                <input type="hidden" name="_method" value="PUT">
                            </div>
                            <div class="col-md-8">
                                <input type="submit" value="{{ __('main.update') }}" class="btn btn-success btn-lg btn-block">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
</div>