<div class="col-md-12">
    {!! Form::open([
        'route'  => ['account.update', 'settings-password'],
        'method' => 'POST',
        'class'  => 'needs-validation'
    ]) !!}
        <div class="card">
            <div class="card-body">
                <div class="row">

                    <div class="col-lg-4">
                        <h4 class="text-center font-italic font-weight-light">{{ __('main.password') }}</h4>
                    </div>

                    <div class="col-lg-8 border-left">
                        <div class="form-group row">
                            <label for="current_password" class="col-sm-4 col-form-label text-md-right">{{ __('main.current_password') }}</label>

                            <div class="col-md-8">
                                <input 
                                    type="password" 
                                    id="current_password" 
                                    name="current_password" 
                                    class="form-control{{ $errors->has('current_password') ? ' is-invalid' : '' }}" 
                                    autocomplete="off"
                                    {{ $errors->has('current_password') ? ' autofocus' : '' }}
                                >

                                @if ($errors->has('current_password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('current_password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <hr>

                        <div class="form-group row">
                            <label for="new_password" class="col-sm-4 col-form-label text-md-right">{{ __('main.new_password') }}</label>

                            <div class="col-md-8">
                                <input 
                                    type="password" 
                                    id="new_password" 
                                    name="new_password" 
                                    class="form-control{{ $errors->has('new_password') ? ' is-invalid' : '' }}" 
                                    autocomplete="off"
                                    {{ $errors->has('new_password') ? ' autofocus' : '' }}
                                >

                                @if ($errors->has('new_password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('new_password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password_confirmation" class="col-sm-4 col-form-label text-md-right">{{ __('main.password_confirmation') }}</label>

                            <div class="col-md-8">
                                <input 
                                    type="password" 
                                    id="password_confirmation" 
                                    name="password_confirmation" 
                                    class="form-control" 
                                    autocomplete="off"
                                >
                            </div>
                        </div>

                        <hr>

                        <div class="form-group row">
                            <div class="col-md-4">
                                <input type="hidden" name="_method" value="PUT">
                            </div>
                            <div class="col-md-8">
                                <input type="submit" value="{{ __('main.update') }}" class="btn btn-success btn-lg btn-block">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
</div>