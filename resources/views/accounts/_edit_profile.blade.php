<div class="col-md-12">
    {!! Form::open([
        'route'   => ['account.update', 'settings-profile'], 
        'method'  => 'POST', 
        'class'   => 'needs-validation'
    ]) !!}
        <div class="card">
            <div class="card-body">
                <div class="row">

                    <div class="col-lg-4">
                        <h4 class="text-center font-italic font-weight-light">{{ __('main.profile') }}</h4>
                    </div>

                    <div class="col-lg-8 border-left">
                        <div class="form-group row">
                            <label for="name" class="col-sm-4 col-form-label text-md-right">{{ __('main.name') }}</label>

                            <div class="col-md-8">
                                <input 
                                    type="text" 
                                    id="name" 
                                    name="name" 
                                    value="{{ $errors->has('name') ? old('name') : $user->name }}" 
                                    class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" 
                                    autocomplete="off"
                                    {{ $errors->has('name') ? ' autofocus' : '' }}
                                >

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="username" class="col-sm-4 col-form-label text-md-right">{{ __('main.username') }}</label>

                            <div class="col-md-8">
                                <input 
                                    type="text" 
                                    id="username" 
                                    name="username" 
                                    value="{{ $errors->has('username') ? old('username') : $user->username }}" 
                                    class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" 
                                    autocomplete="off"
                                    {{ $errors->has('username') ? ' autofocus' : '' }}
                                >

                                @if ($errors->has('username'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <hr>

                        <div class="form-group row">
                            <label for="current_location" class="col-sm-4 col-form-label text-md-right">{{ __('main.current_location') }}</label>

                            <div class="col-md-8">
                                <input 
                                    type="text" 
                                    id="current_location" 
                                    name="current_location" 
                                    value="{{ $errors->has('current_location') ? old('current_location') : $user->current_location }}" 
                                    class="form-control{{ $errors->has('current_location') ? ' is-invalid' : '' }}" 
                                    autocomplete="off"
                                    {{ $errors->has('current_location') ? ' autofocus' : '' }}
                                >

                                @if ($errors->has('current_location'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('current_location') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="interest" class="col-sm-4 col-form-label text-md-right">{{ __('main.interests') }}</label>

                            <div class="col-md-8">
                                <input 
                                    type="text" 
                                    id="interest" 
                                    name="interest" 
                                    value="{{ $errors->has('interest') ? old('interest') : $user->interest }}" 
                                    class="form-control{{ $errors->has('interest') ? ' is-invalid' : '' }}" 
                                    autocomplete="off"
                                    {{ $errors->has('interest') ? ' autofocus' : '' }}
                                >

                                @if ($errors->has('interest'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('interest') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="occupation" class="col-sm-4 col-form-label text-md-right">{{ __('main.occupation') }}</label>

                            <div class="col-md-8">
                                <input 
                                    type="text" 
                                    id="occupation" 
                                    name="occupation" 
                                    value="{{ $errors->has('occupation') ? old('occupation') : $user->occupation }}" 
                                    class="form-control{{ $errors->has('occupation') ? ' is-invalid' : '' }}" 
                                    autocomplete="off"
                                    {{ $errors->has('occupation') ? ' autofocus' : '' }}
                                >

                                @if ($errors->has('occupation'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('occupation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <hr>

                        <div class="form-group row">
                            <label for="twitter" class="col-sm-4 col-form-label text-md-right">{{ __('main.twitter') }}</label>

                            <div class="col-md-8">
                                <input 
                                    type="text" 
                                    id="twitter" 
                                    name="twitter" 
                                    value="{{ $errors->has('twitter') ? old('twitter') : $user->twitter }}" 
                                    class="form-control{{ $errors->has('twitter') ? ' is-invalid' : '' }}" 
                                    autocomplete="off"
                                    {{ $errors->has('twitter') ? ' autofocus' : '' }}
                                >

                                @if ($errors->has('twitter'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('twitter') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="discord" class="col-sm-4 col-form-label text-md-right">{{ __('main.discord') }}</label>

                            <div class="col-md-8">
                                <input 
                                    type="text" 
                                    id="discord" 
                                    name="discord" 
                                    value="{{ $errors->has('discord') ? old('discord') : $user->discord }}" 
                                    class="form-control{{ $errors->has('discord') ? ' is-invalid' : '' }}" 
                                    autocomplete="off"
                                    {{ $errors->has('discord') ? ' autofocus' : '' }}
                                >

                                @if ($errors->has('discord'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('discord') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="skype" class="col-sm-4 col-form-label text-md-right">{{ __('main.skype') }}</label>

                            <div class="col-md-8">
                                <input 
                                    type="text" 
                                    id="skype" 
                                    name="skype" 
                                    value="{{ $errors->has('skype') ? old('skype') : $user->skype }}" 
                                    class="form-control{{ $errors->has('skype') ? ' is-invalid' : '' }}" 
                                    autocomplete="off"
                                    {{ $errors->has('skype') ? ' autofocus' : '' }}
                                >

                                @if ($errors->has('skype'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('skype') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="website" class="col-sm-4 col-form-label text-md-right">{{ __('main.website') }}</label>

                            <div class="col-md-8">
                                <input 
                                    type="text" 
                                    id="website" 
                                    name="website" 
                                    value="{{ $errors->has('website') ? old('website') : $user->website }}" 
                                    class="form-control{{ $errors->has('website') ? ' is-invalid' : '' }}" 
                                    autocomplete="off"
                                    {{ $errors->has('website') ? ' autofocus' : '' }}
                                >

                                @if ($errors->has('website'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('website') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <hr>

                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label text-md-right">{{ __('main.about_me') }}</label>

                            <div class="col-md-8">
                                <textarea id="article-ckeditor" name="bio" class="form-control">{{ $user->bio }}</textarea>

                                @if ($errors->has('bio'))
                                    <div class="invalid-feedback">
                                        {!! $errors->first('bio') !!}
                                    </div>
                                @endif 
                            </div>
                        </div>

                        <hr>

                        <div class="form-group row">
                            <div class="col-md-4">
                                <input type="hidden" name="_method" value="PUT">
                            </div>
                            <div class="col-md-8">
                                <input type="submit" value="{{ __('main.update') }}" class="btn btn-success btn-lg btn-block">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
</div>