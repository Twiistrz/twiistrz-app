<div class="col-md-12">
    {!! Form::open([
        'route'  => ['account.update', 'settings-avatar'],
        'method' => 'POST',
        'class'  => 'needs-validation',
        'enctype' => 'multipart/form-data',
    ]) !!}
        <div class="card">
            <div class="card-body">
                <div class="row">

                    <div class="col-lg-4">
                        <h4 class="text-center font-italic font-weight-light">{{ __('main.avatar') }}</h4>
                    </div>

                    <div class="col-lg-8 border-left">
                        <div class="form-group">
                            <center>
                                <div class="figure">
                                    @if (empty($user->avatar))
                                        @php
                                            $user->avatar = "data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22200%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20200%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_163a43326ef%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_163a43326ef%22%3E%3Crect%20width%3D%22200%22%20height%3D%22200%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2274.4296875%22%20y%3D%22104.5%22%3E200x200%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E";
                                        @endphp
                                    @else
                                        @php
                                            $user->avatar = url('/storage/avatar/' . $user->avatar);
                                        @endphp
                                    @endif
                                    <img style="background-image: url('{{ $user->avatar }}');" class="figure-image mx-auto d-block avatar-profile border">
                                </div>
                            </center>
                        </div>

                        <div class="form-group input-group">
                            <div class="custom-file">
                                <input type="file" name="avatar" class="custom-file-input">
                                <label class="custom-file-label" for="avatar">{{ __('main.choose_avatar') }}</label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <input type="hidden" name="_method" value="PUT">
                                <input type="submit" value="{{ __('main.update') }}" class="btn btn-success btn-lg btn-block">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
</div>