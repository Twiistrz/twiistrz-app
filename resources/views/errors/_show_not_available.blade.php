<div class="jumbotron text-center">
    <h1 class="display-4">{{ __('main.this_content_is_currently_unavailable') }}</h1>
    <p class="lead">{{ __('main.the_page_you_request_cannot_be_displayed_right_now') }}</p>
    <p class="lead">
        <a href="{{ route('home') }}" class="btn btn-primary btn-lg">{{ __('main.home') }}</a>
        <a href="{{ route('news.home') }}" class="btn btn-primary btn-lg">{{ __('main.news') }}</a>
        <a href="{{ url('https://github.com/Twiistrz/CRUD-using-Laravel/issues') }}" class="btn btn-light btn-lg">{{ __('main.new_issue') }}</a>
    </p>
</div>