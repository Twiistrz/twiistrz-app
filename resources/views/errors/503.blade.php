@extends('master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            @include('errors._show_not_available')
        </div>
    </div>
</div>
@endsection
