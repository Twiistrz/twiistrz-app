@extends('master')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <h3 class="text-center font-italic font-weight-light">{{ __('main.create_post') }}</h3>
                <hr>
                {!! Form::open([
                    'route'   => 'post.publish', 
                    'method'  => 'POST',
                    'class'  => 'needs-validation',
                    'enctype' => 'multipart/form-data',
                ]) !!}
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">{{ __('main.post_title') }}</span>
                        </div>
                        <input 
                            type="text" 
                            name="title" 
                            value="{{ $errors->has('title') ? old('title') : '' }}"
                            class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" 
                            autocomplete="off"
                            {{ $errors->has('title') ? 'autofocus' : '' }}
                        >

                        @if ($errors->has('title'))
                            <div class="invalid-feedback">
                                {!! $errors->first('title') !!}
                            </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <textarea id="article-ckeditor" name="body" class="form-control"></textarea>

                        @if ($errors->has('body'))
                            <div class="invalid-feedback">
                                {!! $errors->first('body') !!}
                            </div>
                        @endif
                    </div>

                    <div class="form-group input-group">
                        <div class="custom-file">
                            <input type="file" name="cover_image" class="custom-file-input">
                            <label class="custom-file-label" for="cover_image">{{ __('main.choose_cover_image') }}</label>
                        </div>
                    </div>

                    <input type="submit" value="{{ __('main.publish_post') }}" class="btn btn-success btn-lg float-right">
                {!! Form::close() !!} 
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>CKEDITOR.replace('article-ckeditor');</script>
    @if (count($errors) > 0) 
        @include('errors._show_swal_error')  
    @endif
@endsection