@extends('master')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                @if ($post)
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('news.home') }}">{{ __('main.news') }}</a></li>
                            @if ($post)
                                <li class="breadcrumb-item active" aria-current="page">{{ $post->title }}</li>
                            @endif
                        </ol>
                    </nav>
                    <div class="text-center">
                        <h3 class="text-primary">{{ $post->title }}</h3>
                        Published {{ \Carbon\Carbon::createFromTimeStamp(strtotime($post->created_at))->diffForHumans() }} by <a href="{{ route('account.profile', $post->user->id) }}">{{ $post->user->username }}</a>
                    </div>
                    <hr>
                    <div>
                        @if ($post->cover_image !== null)
                            <div class="text-center">
                                <figure class="figure">
                                    <img class="figure-img img-fluid rounded" src="{{ url('/storage/posts/' . $post->cover_image) }}" alt="{{ $post->cover_image }}">
                                </figure>
                            </div>
                        @endif
                        {!! $post->body !!}
                    </div>
                    @if (!Auth::guest())
                        @if (Auth::user()->id == $post->user_id || $user->hasAnyRole(['Developer','Administrator']))

                            <hr>
                            <div class="float-right">
                                <a href="{{ route('post.edit', $post->id) }}" class="btn btn-primary btn-lg">{{ __('main.edit_post') }}</a>
                                <a href="{{ route('post.delete', $post->id) }}" class="btn btn-danger btn-lg"
                                    onclick="
                                        event.preventDefault();
                                        document.getElementById('delete-post').submit();"
                                >
                                    {{ __('main.delete_post') }}
                                </a>

                                {!! Form::open([
                                    'id'     => 'delete-post',
                                    'route'  => ['post.delete', $post->id],
                                    'method' => 'POST', 
                                    'style'  => 'display: none;',
                                ]) !!}
                                    <input type="hidden" name="_method" value="DELETE">
                                {!! Form::close() !!}
                            </div>
                        @endif
                    @endif
                @else
                    @include('errors._show_not_available')
                @endif

            </div>
        </div>
    </div>
@endsection