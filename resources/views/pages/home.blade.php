@extends('master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="jumbotron jumbotron-fluid text-center">
                <h1 class="display-4 text-dark">{{ __('welcome to twiistrz app!') }}</h1>
                <p class="lead text-dark font-italic">{{ __('browse the website to learn more') }}</p>
                <p class="lead">
                    Test initial
                    <a href="{{ url('https://github.com/Twiistrz/CRUD-using-Laravel') }}" class="btn btn-primary btn-lg" style="background-color: #4078c0; border-color: #3565a3;" target="_blank">{{ __('Fork me on GitHub') }}</a>
                    <a href="{{ url('https://www.facebook.com/Twiistrz') }}" class="btn btn-primary btn-lg" style="background-color: #3B5998; border-color: #2b4272;" target="_blank">{{ __('Facebook') }}</a>
                    <a href="{{ url('mailto:bsit.emmanuel@gmail.com') }}" class="btn btn-primary btn-lg" style="background-color: #ff4343; border-color: #d13a3a;" target="_blank">{{ __('Email') }}</a>
                </p>
            </div>
        </div>
    </div>
</div>
@endsection
