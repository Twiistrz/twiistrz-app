@extends('master')

@section('content')
    <div class="container">
        <h3 class="text-center font-italic font-weight-light">{{ __('Dashboard') }}</h3>
        <hr>
        
        <div class="card">
            <div class="card-body">
                <h5 class="card-title text-center font-italic font-weight-light">{{ __('Your Posts') }}</h5>
                <hr>
                <a href="{{ route('post.new') }}" class="btn btn-success btn-lg btn-block">{{ __('main.new_post') }}</a>
                <p class="card-text">
                    @if (count($posts) > 0)
                        <table class="table table-sm table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th class="font-italic">{{ __('Title') }}</th>
                                    <th class="font-italic" style="width:110px">{{ __('Published On') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($posts as $post)
                                    <tr>
                                        <td><a href="{{ route('post.view', $post->id) }}" target="_blank">{{ $post->title }}</a></td>
                                        <td>{{ \Carbon\Carbon::parse($post->created_at)->format('M d, Y')}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">{{ $posts->links() }}</div>
                    @else
                        <div class="alert alert-info">{{ __('No post found.') }}</div>
                    @endif
                </p>
            </div>
        </div>
    </div>
@endsection
