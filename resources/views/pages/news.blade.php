@extends('master')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <h3 class="text-center font-italic font-weight-light">{{ __('main.c_news') }}</h3>
                <hr>
                @if (count($posts) > 0)
                    @foreach ($posts as $post)
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title"><a href="{{ route('post.view', $post->id) }}">{{ $post->title }}</a></h4>
                                <h6 class="card-subtitle mb-2 text-muted">
                                    {{ \Carbon\Carbon::createFromTimeStamp(strtotime($post->created_at))->diffForHumans() }} by <a href="{{ route('account.profile', $post->user->id) }}">{{ $post->user->username }}</a>
                                </h6>
                            </div>
                        </div>
                    @endforeach
                    {{ $posts->links() }}
                @else
                    <div class="jumbotron text-center">
                        <h1 class="display-4">{{ __('main.this_content_is_currently_unavailable') }}</h1>
                        <p class="lead">{{ __('main.no_content_available_right_now') }}</p>
                        <p class="lead">
                            <a href="{{ route('home') }}" class="btn btn-primary btn-lg">{{ __('main.home') }}</a>
                            <a href="{{ route('news.home') }}" class="btn btn-primary btn-lg">{{ __('main.news') }}</a>
                            <a href="{{ url('https://github.com/Twiistrz/CRUD-using-Laravel/issues') }}" class="btn btn-light btn-lg">{{ __('main.new_issue') }}</a>
                        </p>
                    </div>
                @endif                
            </div>
        </div>
    </div>
@endsection
