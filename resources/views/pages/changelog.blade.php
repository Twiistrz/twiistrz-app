@extends('master')

@section('content')
<div class="container">
    <div class="row justify-content-center">        
        @include('layouts._show_changelogs')
    </div>
</div>
@endsection