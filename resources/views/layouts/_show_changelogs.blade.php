<div class="col-md-10">
    <h3 class="text-center font-italic font-weight-light">{{ __('main.c_changelog') }}</h3>
    <hr>
    <div class="list-group">
        <a href="#v3-0-0" class="list-group-item list-group-item-action flex-column align-items-start" id="v3-0-0">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1"><span class="badge badge-pill badge-info">{{ __('Initial Release') }}</span> <span class="badge badge-pill badge-success">{{ __('New') }}</span> {{ __('Version 3.0.0') }}</h5>
            </div>
            <p class="mb-1">
                <ul>
                    <li>{{ __('Initial Release of Version 3') }}</li>
                    <li>{{ __('Many Improvements') }}</li>
                </ul>
            </p>
        </a>

        <a href="#v2-7-5" class="list-group-item list-group-item-action flex-column align-items-start" id="v2-7-5">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1"><span class="badge badge-pill badge-warning">{{ __('Hotfix') }}</span> {{ __('Version 2.7.5') }}</h5>
            </div>
            <p class="mb-1">
                <ul>
                    <li>{{ __('Update: Views') }}</li>
                    <li>{{ __('Update: Controllers') }}</li>
                    <li>{{ __('Addition: Laravel Icon') }}</li>
                    <li>{{ __('Fix: Known Bugs') }}</li>
                </ul>
            </p>
        </a>

        <a href="#v2-7-1" class="list-group-item list-group-item-action flex-column align-items-start" id="v2-7-1">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">{{ __('Version 2.7.1') }}</h5>
            </div>
            <p class="mb-1">
                <ul>
                    <li>{{ __('Update: Views') }}</li>
                    <li>{{ __('Update: Controllers') }}</li>
                    <li>{{ __('Update: Register') }}</li>
                    <li>{{ __('Update: Migration') }}</li>
                    <li>{{ __('Update: Seeder') }}</li>
                    <li>{{ __('Update: Language File') }}</li>
                    <li>{{ __('Addition: Roles') }}</li>
                    <li>{{ __('Addition: Permissions') }}</li>
                    <li>{{ __('Fix: Known Bugs') }}</li>
                </ul>
            </p>
        </a>

        <a href="#v2-6-2" class="list-group-item list-group-item-action flex-column align-items-start" id="v2-6-2">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">{{ __('Version 2.6.2') }}</h5>
            </div>
            <p class="mb-1">
                <ul>
                    <li>{{ __('Update: Views') }}</li>
                    <li>{{ __('Update: Controllers') }}</li>
                    <li>{{ __('Update: Routes') }}</li>
                    <li>{{ __('Update: Style') }}</li>
                    <li>{{ __('Update: Home Page') }}</li>
                    <li>{{ __('Update: News Page') }}</li>
                    <li>{{ __('Update: Profile Page') }}</li>
                    <li>{{ __('Update: Settings Page') }}</li>
                    <li>{{ __('Addition: Changelog Page') }}</li>
                    <li>{{ __('Addition: Update Email') }}</li>
                    <li>{{ __('Addition: Language File') }}</li>
                    <li>{{ __('Fix: Known Bugs') }}</li>
                </ul>
            </p>
        </a>
        
        <a href="#v2-5-0" class="list-group-item list-group-item-action flex-column align-items-start" id="v2-5-0">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1"><span class="badge badge-pill badge-warning">{{ __('Hotfix') }}</span> {{ __('Version 2.5.0') }}</h5>
            </div>
            <p class="mb-1">
                <ul>
                    <li>{{ __('Update Views') }}</li>
                    <li>{{ __('Update Controllers') }}</li>
                    <li>{{ __('Update Sources') }}</li>
                    <li>{{ __('Update Profile') }}</li>
                    <li>{{ __('Update Settings') }}</li>
                    <li>{{ __('Add Avatar') }}</li>
                    <li>{{ __('Fixed Bugs') }}</li>
                </ul>
            </p>
        </a>

        <a href="#v2-4-3" class="list-group-item list-group-item-action flex-column align-items-start" id="v2-4-3">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">{{ __('Version 2.4.3') }}</h5>
            </div>
            <p class="mb-1">
                <ul>
                    <li>{{ __('Update Views') }}</li>
                    <li>{{ __('Update Controllers') }}</li>
                    <li>{{ __('Update Error Pages') }}</li>
                    <li>{{ __('Fixed Bugs') }}</li>
                </ul>
            </p>
        </a>

        <a href="#v2-3-9" class="list-group-item list-group-item-action flex-column align-items-start" id="v2-3-9">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">{{ __('Version 2.3.9') }}</h5>
            </div>
            <p class="mb-1">
                <ul>
                    <li>{{ __('Update Kernel and Controllers') }}</li>
                    <li>{{ __('Update Blog Design') }}</li>
                    <li>{{ __('Add Update Password with Validation') }}</li>
                    <li>{{ __('Fixed Bugs') }}</li>
                </ul>
            </p>
        </a>

        <a href="#v2-3-5" class="list-group-item list-group-item-action flex-column align-items-start" id="v2-3-5">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">{{ __('Version 2.3.5') }}</h5>
            </div>
            <p class="mb-1">
                <ul>
                    <li>{{ __('Update Routes') }}</li>
                    <li>{{ __('Update Outsourcing Blog Design') }}</li>
                    <li>{{ __('Update Redirects in PagesController') }}</li>
                    <li>{{ __('Add Encryption of Cover Image Filename') }}</li>
                    <li>{{ __('Add Deleting of Cover Image') }}</li>
                    <li>{{ __('Fixed Bugs') }}</li>
                </ul>
            </p>
        </a>

        <a href="#v2-2-9" class="list-group-item list-group-item-action flex-column align-items-start" id="v2-2-9">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">{{ __('Version 2.2.9') }}</h5>
            </div>
            <p class="mb-1">
                <ul>
                    <li>{{ __('Update Routes') }}</li>
                    <li>{{ __('Update URL\'s') }}</li>
                    <li>{{ __('Update Account Settings') }}</li>
                    <li>{{ __('Update ENV File') }}</li>
                    <li>{{ __('Fixed Bugs') }}</li>
                </ul>
            </p>
        </a>

        <a href="#v2-2-4" class="list-group-item list-group-item-action flex-column align-items-start" id="v2-2-4">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">{{ __('Version 2.2.4') }}</h5>
            </div>
            <p class="mb-1">
                <ul>
                    <li>{{ __('Update "My Profile" Design') }}</li>
                    <li>{{ __('Update "Account Settings"') }}</li>
                    <li>{{ __('Add "Bio" to database') }}</li>
                    <li>{{ __('Fixed Bugs') }}</li>
                </ul>
            </p>
        </a>

        <a href="#v2-2-0" class="list-group-item list-group-item-action flex-column align-items-start" id="v2-2-0">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">{{ __('Version 2.2.0') }}</h5>
            </div>
            <p class="mb-1">
                <ul>
                    <li>{{ __('Update Dashboard Design') }}</li>
                    <li>{{ __('Update Changelog Design') }}</li>
                    <li>{{ __('Add "My Profile"') }}</li>
                    <li>{{ __('Add "Update Profile" in Account Settings') }}</li>
                    <li>{{ __('Fixed Bugs') }}</li>
                </ul>
            </p>
        </a>

        <a href="#v2-0-4" class="list-group-item list-group-item-action flex-column align-items-start" id="v2-0-4">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">{{ __('Version 2.0.4') }}</h5>
            </div>
            <p class="mb-1">
                <ul>
                    <li>{{ __('Fixed Typo') }}</li>
                    <li>{{ __('Fixed Minor Bugs') }}</li>
                    <li>{{ __('Update 404 Error Page') }}</li>
                    <li>{{ __('Add View Post in Edit') }}</li>
                </ul>
            </p>
        </a>

        <a href="#v2-0-0" class="list-group-item list-group-item-action flex-column align-items-start" id="v2-0-0">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1"><span class="badge badge-pill badge-info">{{ __('Initial Release') }}</span> {{ __('Version 2.0.0') }}</h5>
            </div>
            <p class="mb-1">
                <ul>
                    <li>{{ __('Initial Release of Version 2') }}</li>
                    <li>{{ __('Many Improvements') }}</li>
                </ul>
            </p>
        </a>

        <a href="#v1-4-2" class="list-group-item list-group-item-action flex-column align-items-start" id="v1-4-2">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">{{ __('Version 1.4.2') }}</h5>
            </div>
            <p class="mb-1">
                <ul>
                    <li>{{ __('Fixed Errors and Bugs') }}</li>
                </ul>
            </p>
        </a>

        <a href="#v1-1-0" class="list-group-item list-group-item-action flex-column align-items-start" id="v1-1-0">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">{{ __('Version 1.1.0') }}</h5>
            </div>
            <p class="mb-1">
                <ul>
                    <li>{{ __('Add Login and Register') }}</li>
                </ul>
            </p>
        </a>

        <a href="#v1-0-5" class="list-group-item list-group-item-action flex-column align-items-start" id="v1-0-5">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">{{ __('Version 1.0.5') }}</h5>
            </div>
            <p class="mb-1">
                <ul>
                    <li>{{ __('Fixed Bugs') }}</li>
                </ul>
            </p>
        </a>

        <a href="#v1-0-0" class="list-group-item list-group-item-action flex-column align-items-start" id="v1-0-0">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1"><span class="badge badge-pill badge-info">{{ __('Initial Release') }}</span> {{ __('Version 1.0.0') }}</h5>
            </div>
            <p class="mb-1">
                <ul>
                    <li>{{ __('Initial Release of Version 1') }}</li>
                </ul>
            </p>
        </a>
    </div>
</div>