<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand mb-0 h1" href="{{ route('home') }}">
            {{ config('app.name', 'Twiistrz') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link{{ Request::is('/') ? ' active' : '' }}" href="{{ route('home') }}">{{ __('main.home') }}{!! Request::is('/') ? '<span class="sr-only">(current)</span>' : '' !!}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link{{ Request::is('news') ? ' active' : '' }}" href="{{ route('news.home') }}">{{ __('main.news') }}{!! Request::is('news') ? '<span class="sr-only">(current)</span>' : '' !!}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link{{ Request::is('changelog') ? ' active' : '' }}" href="{{ route('changelog') }}">{{ __('main.changelog') }}{!! Request::is('changelog') ? '<span class="sr-only">(current)</span>' : '' !!}</a>
                </li>
            </ul>

            <ul class="navbar-nav ml-auto">
                @guest
                    <li><a class="nav-link" href="{{ route('login') }}">{{ __('login') }}</a></li>
                    <li><a class="nav-link" href="{{ route('register') }}">{{ __('register') }}</a></li>
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('account.profile', Auth::user()->id) }}">{{ __('main.my_profile') }}</a>
                            <a class="dropdown-item" href="{{ route('account.settings') }}">{{ __('main.settings') }}</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('dashboard') }}">{{ __('main.dashboard') }}</a>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="
                                    event.preventDefault();
                                    document.getElementById('logout-form').submit();"
                            >
                                {{ __('main.logout') }}
                            </a>

                            {!! Form::open([
                                'id'     => 'logout-form',
                                'route'  => 'logout',
                                'method' => 'POST', 
                                'style'  => 'display: none;',
                            ]) !!}
                                @csrf
                            {!! Form::close() !!}
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>