@extends('master')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title text-center">{{ __('Account Login') }}</h4>
                        <hr>
                        {!! Form::open([
                            'route'   => 'login', 
                            'method'  => 'POST',
                        ]) !!}
                            @csrf

                            <div class="form-group row">
                                <label for="username" class="col-sm-4 col-form-label text-md-right">{{ __('username') }}</label>

                                <div class="col-md-6">
                                    <input 
                                        type="text" 
                                        name="username" 
                                        value="{{ old('username') }}" 
                                        class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" 
                                        autocomplete="off"
                                        {{ $errors->has('username') ? ' autofocus' : '' }}
                                    >

                                    @if ($errors->has('username'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('username') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('password') }}</label>
                                <div class="col-md-6">
                                    <input 
                                        type="password" 
                                        name="password" 
                                        class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" 
                                        autocomplete="off"
                                        {{ $errors->has('password') ? ' autofocus' : '' }}
                                    >
                
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input 
                                                type="checkbox" 
                                                name="remember" 
                                                class="form-check-input"
                                                {{ old('remember') ? 'checked' : '' }}
                                            >{{ __('remember me') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <input type="submit" class="btn btn-primary btn-lg" value="{{ __('Login') }}">
                                    <a class="btn btn-link btn-lg" href="{{ route('password.request') }}">
                                        {{ __('Forgot Password?') }}
                                    </a>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if (count($errors) > 0) 
        @include('errors._show_swal_error')
    @endif
@endsection
