<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.metadata')
    <title>{{ config('app.name', 'Twiistrz') }}</title>
</head>
<body>
    <div id="app">
        @include('layouts._show_navigation')

        <main class="py-4">
            @yield('content')
        </main>
    </div>

    @yield('script')
</body>
</html>
