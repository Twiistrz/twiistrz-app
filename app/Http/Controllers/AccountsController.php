<?php

namespace App\Http\Controllers;

use Alert;
use App\Post;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class AccountsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show']]);
        Validator::extend('check_hashed_pass', function ($attribute, $value, $parameters) {
            return Hash::check($value, $parameters[0]) ? true : false;
        });
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $account = User::find($id);

        if ($account) {
            return view('accounts.show')
                ->with('user', $account)
                ->with('posts', Post::where('user_id', $account->id)->get());
        }

        return view('accounts.show')->with('user', $account);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function editSettings()
    {
        return view('accounts.edit')->with('user', auth()->check() ? User::find(auth()->user()->id) : null);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $action)
    {
        $account = User::where('id', auth()->user()->id)->first();

        if ($action == 'settings-profile') {
            if (strtolower($request->input('username')) !== strtolower(auth()->user()->username)) {
                $this->validate($request, ['username' => 'required|max:255|unique:users']);
            }

            $account->name = $request->input('name');
            $account->username = $request->input('username');
            $account->current_location = $request->input('current_location');
            $account->interest = $request->input('interest');
            $account->occupation = $request->input('occupation');
            $account->twitter = $request->input('twitter');
            $account->discord = $request->input('discord');
            $account->skype = $request->input('skype');
            $account->website = $request->input('website');
            $account->bio = $request->input('bio');
            $account->save();

            alert()
                ->success('Profile updated successfully!', null)
                ->autoClose(2000)
                ->toToast('top-right');
        }

        if ($action == 'settings-password') {
            $this->validate($request, [
                'current_password' => 'required|string|min:6|check_hashed_pass:'.$account->password,
                'new_password'     => 'required|string|min:6|same:password_confirmation',
            ]);

            $account->password = Hash::make($request->input('new_password'));
            $account->save();

            alert()
                ->success('Password updated successfully!', null)
                ->autoClose(2000)
                ->toToast('top-right');
        }

        if ($action == 'settings-avatar') {
            $this->validate($request, ['avatar' => 'image|nullable|max:1999']);

            if ($request->hasFile('avatar')) {
                if (!empty($account->avatar)) {
                    Storage::delete('public/avatar/'.$account->avatar);
                }

                $fileNameWithExt = $request->file('avatar')->getClientOriginalName();
                $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                $fileExt = $request->file('avatar')->getClientOriginalExtension();
                $fileNameToStore = 'user-'.auth()->user()->id.'/'.md5($fileName.time()).'.'.$fileExt;
                $path = $request->file('avatar')->storeAs('public/avatar', $fileNameToStore);

                $account->avatar = $fileNameToStore;
                $account->save();

                alert()
                    ->success('Avatar updated successfully!', null)
                    ->autoClose(2000)
                    ->toToast('top-right');
            } else {
                alert()
                    ->info('Nothing to update!', null)
                    ->autoClose(2000)
                    ->toToast('top-right');
            }
        }

        if ($action == 'settings-email') {
            $this->validate($request, [
                'current_password_email' => 'required|string|min:6|check_hashed_pass:'.$user->password,
                'new_email'              => 'required|string|min:6|same:email_confirmation',
            ]);

            $user->email = $request->input('new_email');
            $user->save();

            alert()
                ->success('Email updated successfully!', null)
                ->autoClose(2000)
                ->toToast('top-right');
        }

        return redirect(route('account.settings'));
    }
}
