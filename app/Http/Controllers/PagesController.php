<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;

class PagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['home', 'news', 'changelog']]);
    }

    /**
     * Show the application home.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        return view('pages.home');
    }

    /**
     * Show the application news.
     *
     * @return \Illuminate\Http\Response
     */
    public function news()
    {
        return view('pages.news')->with('posts', Post::orderBy('created_at', 'desc')->paginate(10));
    }

    /**
     * Show the application changelog.
     *
     * @return \Illuminate\Http\Response
     */
    public function changelog()
    {
        return view('pages.changelog');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        return view('pages.dashboard')->with('posts', Post::where('user_id', User::find(auth()->user()->id))->orderBy('created_at', 'desc')->paginate(10));
    }
}
