<?php

namespace App\Http\Controllers;

use Alert;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = new Post();

        $this->validate($request, [
            'title'       => 'required',
            'body'        => 'required',
            'cover_image' => 'image|nullable|max:1999',
        ]);

        if ($request->hasFile('cover_image')) {
            $fileNameWithExt = $request->file('cover_image')->getClientOriginalName();
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $fileExt = $request->file('cover_image')->getClientOriginalExtension();
            $fileNameToStore = md5($fileName.time()).'.'.$fileExt;
            $path = $request->file('cover_image')->storeAs('public/posts', $fileNameToStore);
        } else {
            $fileNameToStore = null;
        }

        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->user_id = auth()->user()->id;
        $post->cover_image = $fileNameToStore;
        $post->save();

        alert()
            ->success('Post published successfully!', null)
            ->autoClose(2000)
            ->toToast('top-right');

        return redirect(route('news.home'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('posts.show')
            ->with('post', Post::find($id))
            ->with('user', auth()->check() ? User::find(auth()->user()->id) : null);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        alert()
            ->error('Oops... Something went wrong!', null)
            ->autoClose(2000)
            ->toToast('top-right');

        $post = Post::find($id);
        $user = auth()->check() ? User::find(auth()->user()->id) : null;

        return view('posts.edit')
            ->with('post', $post)
            ->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = new Post();
        $post = Post::find($id);
        $user = auth()->check() ? User::find(auth()->user()->id) : null;

        if (auth()->user()->id == $post->user_id || $user->hasAnyRole(['Developer', 'Administrator'])) {
            $this->validate($request, [
                'title'       => 'required|max:255',
                'body'        => 'required|max:255',
                'cover_image' => 'image|nullable|max:1999',
            ]);

            if ($request->hasFile('cover_image')) {
                if (!empty($post->cover_image)) {
                    Storage::delete('public/posts/'.$post->cover_image);
                }

                $fileNameWithExt = $request->file('cover_image')->getClientOriginalName();
                $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                $fileExt = $request->file('cover_image')->getClientOriginalExtension();
                $fileNameToStore = md5($fileName.time()).'.'.$fileExt;
                $path = $request->file('cover_image')->storeAs('public/posts', $fileNameToStore);
            }

            $post->title = $request->input('title');
            $post->body = $request->input('body');
            $post->cover_image = $request->hasFile('cover_image') ? $fileNameToStore : $post->cover_image;
            $post->save();

            alert()
                ->success('Post updated successfully!', null)
                ->autoClose(2000)
                ->toToast('top-right');
        }

        return redirect(route('post.edit', $post->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($post_id)
    {
        $post = new Post();
        $post = Post::where('id', $post_id)->first();
        $user = auth()->check() ? User::find(auth()->user()->id) : null;

        if (auth()->user()->id == $post->user_id || $user->hasAnyRole(['Developer', 'Administrator'])) {
            if (!empty($post->cover_image)) {
                Storage::delete('public/posts/'.$post->cover_image);
            }

            $post->delete();

            alert()
                ->success('Post deleted successfully!', null)
                ->autoClose(2000)
                ->toToast('top-right');

            return redirect(route('news.home'));
        }

        return redirect(route('post.view', $post->id));
    }
}
