<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
    public $primaryKey = 'id';
    public $timestamp = true;

    public function users()
    {
        return $this->belongsToMany('App\User', 'user_role', 'role_id', 'user_id');
    }

    public function posts()
    {
        return $this->belongsToMany('App\Post', 'user_role', 'role_id', 'user_id');
    }
}
