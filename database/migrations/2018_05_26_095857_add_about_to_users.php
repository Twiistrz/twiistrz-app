<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AddAboutToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->mediumText('current_location')->nullable();
            $table->mediumText('interest')->nullable();
            $table->mediumText('occupation')->nullable();
            $table->mediumText('twitter')->nullable();
            $table->mediumText('discord')->nullable();
            $table->mediumText('skype')->nullable();
            $table->mediumText('website')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function ($table) {
            $table->dropColumn('current_location', 'interest', 'occupation', 'twitter', 'discord', 'skype', 'website');
        });
    }
}
