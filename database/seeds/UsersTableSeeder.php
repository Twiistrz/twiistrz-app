<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::where('name', 'Developer')->first();
        $user = new User();
        $user->name = 'Default User';
        $user->username = 'developer';
        $user->email = 'developer@twiistrz.localhost';
        $user->password = Hash::make('developer');
        $user->save();
        $user->roles()->attach($role);
    }
}
